# DOCKERFILE

FROM ubuntu:latest

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install curl vim git apache2 python3 ansible sudo
RUN curl -O https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
RUN mkdir /root/gopath
RUN tar -xzvf go1.7.4.linux-amd64.tar.gz
RUN sudo mv go /root/gopath
RUN echo $'export PATH=$PATH:$GOROOT   export GOROOT=/root/go/   export GOPATH=/root/gopath/' >> /etc/profile.d/setup.sh
RUN chmod +x /etc/profile.d/setup.sh
RUN /bin/bash -c "source /etc/profile.d/setup.sh ; mkdir -p /root/gopath/src/github.com/hyperledger"
RUN cd /root/gopath/src/github.com/hyperledger/
RUN git clone https://github.com/hyperledger/fabric.git /root/gopath/src/github.com/hyperledger/clone1
RUN git clone https://github.com/hyperledger/fabric-sdk-node.git /root/gopath/src/github.com/hyperledger/clone2
RUN set -x \
        && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash \
        && export NVM_DIR="$HOME/.nvm" \
        && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" \
        && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" \
        && nvm install lts/boron \
